//
//  SecondViewController.swift
//  UsodeControles
//
//  Created by Emerson on 6/6/20.
//  Copyright © 2020 Emerson. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var controlSegmento: UISegmentedControl!
    @IBOutlet weak var txtNumero1: UITextField!
    @IBOutlet weak var txtNumero2: UITextField!
    @IBOutlet weak var lblRespuesta: UILabel!
    
    var nro1:Double = 0
    var nro2:Double = 0
    
    @IBAction func elegirSegmento(_ sender: Any) {
        if Double(txtNumero1.text!) != nil && Double(txtNumero2.text!) != nil {
            nro1 =  Double(txtNumero1.text!)!
            nro2 =  Double(txtNumero2.text!)!
            let option = controlSegmento.selectedSegmentIndex
            switch option {
            case 0:
                lblRespuesta.text = Operacion(n1: nro1, n2: nro2, op: "+")
            case 1:
            lblRespuesta.text = Operacion(n1: nro1, n2: nro2, op: "-")
            case 2:
            lblRespuesta.text = Operacion(n1: nro1, n2: nro2, op: "*")
            case 3:
            lblRespuesta.text = Operacion(n1: nro1, n2: nro2, op: "/")
            default:
                lblRespuesta.text = "Error"
            }
        }else{
            lblRespuesta.text = "Error"
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(ocultarTeclado))
        view.addGestureRecognizer(tap)
    }
    
    func Operacion(n1:Double, n2:Double, op:String) -> String {
        switch op {
        case "+":
            return String(n1 + n2)
        case "-":
            return String(n1 - n2)
        case "*":
            return String(n1 * n2)
        case "/":
            return String(n1 / n2)
        default:
            return "Error"
        }
    }
    
    @objc func ocultarTeclado() {
        view.endEditing(true)
    }


}

